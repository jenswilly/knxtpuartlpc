// File: KnxTpUart.h
// Author: Daniel Kleine-Albers (Since 2012)
// Modified: Thorsten Gehrig (Since 2014)
// Modified: Michael Werski (Since 2014)
// Modified: Katja Blankenheim (Since 2014)
// Modified: Mag Gyver (Since 2016)

// Last modified: 06.06.2017

#ifndef KnxTpUart_h
#define KnxTpUart_h

#include "lpc_types.h"
#include "KnxTelegram.h"
#include "../HardwareSerial.h"
#include <string>

// Services from TPUART
#define TPUART_RESET_INDICATION_BYTE 0B11

// Services to TPUART
#define TPUART_DATA_START_CONTINUE 0B10000000
#define TPUART_DATA_END 0B01000000

// Uncomment the following line to enable debugging
//#define TPUART_DEBUG

#define TPUART_DEBUG_PORT Serial

#define TPUART_SERIAL_CLASS HardwareSerial

// Delay in ms between sending of packets to the bus
// Change only if you know what you're doing
#define SERIAL_WRITE_DELAY_MS 100

// Timeout for reading a byte from TPUART
// Change only if you know what you're doing
#define SERIAL_READ_TIMEOUT_MS 10

// Maximum number of group addresses that can be listened on
#define MAX_LISTEN_GROUP_ADDRESSES 15

enum KnxTpUartSerialEventType {
  TPUART_RESET_INDICATION,
  KNX_TELEGRAM,
  IRRELEVANT_KNX_TELEGRAM,
  UNKNOWN
};

// Macro functions for conversion of individual (physical) and three level group addresses
inline uint16_t INDIVIDUAL_ADDR( uint8_t area, uint8_t line, uint8_t busdevice )
{
	return (uint16_t) (((area & 0xF) << 12) + ((line & 0xF) << 8) + busdevice);
}

inline uint16_t GROUP_ADDR( uint8_t maingrp, uint8_t midgrp, uint8_t subgrp )
{
	return (uint16_t) (((maingrp & 0x1F) << 11) + ((midgrp & 0x7) << 8) + subgrp);
}

class KnxTpUart
{
  public:
    KnxTpUart( TPUART_SERIAL_CLASS* sport, int source_area, int source_line, int source_member );
    void uartReset();
    void uartStateRequest();
    KnxTpUartSerialEventType serialEvent();
    KnxTelegram* getReceivedTelegram();

    void setIndividualAddress(int, int, int);

    void sendAck();
    void sendNotAddressed();

	bool groupWriteBool( uint16_t groupAddress, bool );
    bool groupWrite4BitInt( uint16_t groupAddress, int);
    bool groupWrite4BitDim( uint16_t groupAddress, bool, uint8_t);
    bool groupWrite1ByteInt( uint16_t groupAddress, int);
    bool groupWrite2ByteInt( uint16_t groupAddress, int);
    bool groupWrite2ByteFloat( uint16_t groupAddress, float);
    bool groupWrite3ByteTime( uint16_t groupAddress, int, int, int, int);
    bool groupWrite3ByteDate( uint16_t groupAddress, int, int, int);
    bool groupWrite4ByteFloat( uint16_t groupAddress, float);
    bool groupWrite14ByteText( uint16_t groupAddress, std::string );

    bool groupAnswerBool( uint16_t groupAddress, bool);
    /*
      bool groupAnswer4BitInt(String, int);
      bool groupAnswer4BitDim(String, bool, byte);
    */
    bool groupAnswer1ByteInt( uint16_t groupAddress, int);
    bool groupAnswer2ByteInt( uint16_t groupAddress, int);
    bool groupAnswer2ByteFloat( uint16_t groupAddress, float);
    bool groupAnswer3ByteTime( uint16_t groupAddress, int, int, int, int);
    bool groupAnswer3ByteDate( uint16_t groupAddress, int, int, int);
    bool groupAnswer4ByteFloat( uint16_t groupAddress, float);
    bool groupAnswer14ByteText( uint16_t groupAddress, std::string);

    bool groupRead( uint16_t groupAddress);

    void addListenGroupAddress( uint16_t groupAddress );
    bool isListeningToGroupAddress(int, int, int);

    bool individualAnswerAddress();
    bool individualAnswerMaskVersion(int, int, int);
    bool individualAnswerAuth(int, int, int, int, int);

    void setListenToBroadcasts(bool);


  private:
    HardwareSerial* _serialport;
    KnxTelegram* _tg;       // for normal communication
    KnxTelegram* _tg_ptp;   // for PTP sequence confirmation
    int _source_area;
    int _source_line;
    int _source_member;
    int _listen_group_addresses[MAX_LISTEN_GROUP_ADDRESSES][3];
    int _listen_group_address_count;
    bool _listen_to_broadcasts;

    bool isKNXControlByte( uint8_t );
    void checkErrors();
    void printByte(uint8_t);
    bool readKNXTelegram();
	void createKNXMessageFrame( int, KnxCommandType, uint16_t groupAddress, int );
	void createKNXMessageFrameIndividual( int, KnxCommandType, uint16_t individualAddress, int );
    bool sendMessage();
    bool sendNCDPosConfirm(int, int, int, int);
    int serialRead();
};

#endif
