# KnxTpUart LPC

This is a fork of Thorsten Gehrig's [arduino-tpuart KNX-User-Forum](https://bitbucket.org/thorstengehrig/arduino-tpuart-knx-user-forum) repository.

The code has been converted from Arduino to MCUXpresso LPC11U68.

To use the code you will need the SysTick timer in the `drivers` directory and one of the `UartXRingBufferSerial` classes from the `serial` directory.

The SysTick driver must be initialized with `SysTick_init( 1000 );` before initializing the KNX class.

Example code:

```C++
HardwareSerial	*serial = new Uart1RingBufferSerial();
SysTick_delay( 500 );

// Initialize KNX
KnxTpUart knx( &serial, 1, 1, 100 );

serial.begin( 19200, SERIAL_8E1 );
knx.uartReset();

knx.addListenGroupAddress( GROUP_ADDR( 1, 0, 0 ) );	// Switch
knx.addListenGroupAddress( GROUP_ADDR( 1, 1, 0 ) ); // Status

while( true ) {
	knx_task( knx );
}
```

and 

```C++
void knx_task( KnxTpUart &knx )
{
	KnxTpUartSerialEventType eType = knx.serialEvent();

	//Evaluation of the received telegram -> only KNX telegrams are accepted
	if( eType == KNX_TELEGRAM ) {
		KnxTelegram* telegram = knx.getReceivedTelegram();
		uint16_t targetGA = GROUP_ADDR( telegram->getTargetMainGroup(), telegram->getTargetMiddleGroup(), telegram->getTargetSubGroup() );

		if( targetGA == GROUP_ADDR( 1, 0, 0 ) || targetGA == GROUP_ADDR( 1, 1, 0 ) ) {
			KnxCommandType command = telegram->getCommand();
			if( command == KNX_COMMAND_ANSWER || command == KNX_COMMAND_WRITE )	// Since we're not explicitly requesting value, it's not going to be KNX_COMMAND_ANSWER
					{
				bool value = telegram->getBool();
				Board_LED_Set( 1, value );
				if( value ) {
					knx.groupWriteBool( GROUP_ADDR( 1,0,1 ), true );
				}
			}
		}
	}
}
```

