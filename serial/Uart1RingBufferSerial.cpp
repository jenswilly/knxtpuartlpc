/*
 * Uart0RingBufferSerial.cpp
 *
 *  Created on: 25 Oct 2019
 *      Author: jenswilly
 */

#include "Uart1RingBufferSerial.h"
#include "board.h"

static RINGBUFF_T txring, rxring;
static uint8_t rxbuff[ UART_RRB_SIZE ], txbuff[ UART_SRB_SIZE ];

/**
 * @brief	UART interrupt handler using ring buffers
 * @return	Nothing
 */
extern "C" {
void USART1_4_IRQHandler(void)
{
	// Use default ring buffer handler.
	Chip_UARTN_IRQRBHandler(LPC_USART1, &rxring, &txring);
}
}

Uart1RingBufferSerial::Uart1RingBufferSerial()
{
	Uart1RingBufferSerial::InitPinMux();
}

Uart1RingBufferSerial::~Uart1RingBufferSerial()
{
	end();
}

void Uart1RingBufferSerial::begin( unsigned long baud, e_SerialMode mode )
{
	// Set clock rate
	Chip_Clock_SetUSARTNBaseClockRate( (115200 * 48), true);

	// Setup UART
	Chip_UARTN_Init(LPC_USART1);
	Chip_UARTN_ConfigData(LPC_USART1, UARTN_CFG_DATALEN_8 | UARTN_CFG_PARITY_EVEN | UARTN_CFG_STOPLEN_1);
	Chip_UARTN_SetBaud(LPC_USART1, baud);

	Chip_UARTN_Enable(LPC_USART1);
	Chip_UARTN_TXEnable(LPC_USART1);

	/* Before using the ring buffers, initialize them using the ring
	   buffer init function */
	RingBuffer_Init(&rxring, rxbuff, 1, UART_RRB_SIZE);
	RingBuffer_Init(&txring, txbuff, 1, UART_SRB_SIZE);

	/* Enable receive data and line status interrupt */
	Chip_UARTN_IntEnable(LPC_USART1, UARTN_INTEN_RXRDY);
	Chip_UARTN_IntDisable(LPC_USART1, UARTN_INTEN_TXRDY);	/* May not be needed */

	/* Enable UART interrupt */
	NVIC_EnableIRQ(USART1_4_IRQn);
}

void Uart1RingBufferSerial::end()
{
	// Clear buffers
	RingBuffer_Flush( &rxring );
	RingBuffer_Flush( &txring );

	// DeInitialize UART0 peripheral
	NVIC_DisableIRQ( USART1_4_IRQn );
	Chip_UARTN_DeInit( LPC_USART1 );
}

/**
 * Returns the number of available bytes in the RX buffer
 */
int Uart1RingBufferSerial::available()
{
	return RingBuffer_GetCount( &rxring );
}

/**
 * Read one byte from the RX buffer.
 *
 * Check if any data is available first using Uart0RingBufferSerial::available().
 * If this method is called with no data available, 0 is returned which is not correct as 0 might be a valid data byte.
 */
uint8_t Uart1RingBufferSerial::read()
{
	uint8_t data = 0;
	Chip_UARTN_ReadRB( LPC_USART1, &rxring, &data, 1 );
	return data;
}

/**
 * Read one byute from the RX buffer _but leave the byte in the buffer_.
 *
 * This function does _not_ check whether any data is available so be sure to use `Uart0RingBufferSerial::available()` first.
 */
uint8_t Uart1RingBufferSerial::peek()
{
	uint8_t data = 0;

	RingBuffer_Peek( &rxring, &data );
	return data;
}

int Uart1RingBufferSerial::write( uint8_t data )
{
	return Chip_UARTN_SendRB( LPC_USART1, &txring, (const uint8_t *)&data, 1 );
}

int Uart1RingBufferSerial::write( uint8_t *buffer, int size )
{
	// No need for the default implementation's for loop: write all bytes at once.
	return Chip_UARTN_SendRB( LPC_USART1, &txring, (const uint8_t *)buffer, size );
}

/**
 * Initializes PINMUX for USART1.
 *
 * The McLean board uses USART1 with the following pins:
 *
 * RX | PIO0.13 | pin 32
 * TX | PIO0.14 | pin 33
 *
 */
void Uart1RingBufferSerial::InitPinMux()
{
#if  defined( BOARD_NXP_LPCXPRESSO_11U68_JWJ )
	// The proto board uses P0.18 and P0.19 which are configured in board_sysint.c, IOCON_Init() so no configuration here.
#else
	// Only The McLean board is supported for now
	#error "No UART setup defined"
#endif
}
